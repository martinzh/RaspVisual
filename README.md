# RaspVisual

Proyecto audio-visual reactivo.

 * Martín Zumaya
 * Eduardo Obieta

## Open Framewroks y Raspberry Pi ##

* [Getting Started](http://openframeworks.cc/setup/raspberrypi/raspberry-pi-getting-started/)
* [Cross Compliling](http://openframeworks.cc/setup/raspberrypi/raspberry-pi-cross-compiling-guide/)
* [ofx Sound Addons](http://ofxaddons.com/categories/5-sound)
* [ofx MIDI addon](https://github.com/danomatika/ofxMidi)
    - Agregar la línea `#include "ofxMidiConstants.h"` en `RtMidi.h`.
* [RTMidi](http://www.music.mcgill.ca/%7Egary/rtmidi/)
* [ofx MIDI addon](https://github.com/danomatika/ofxMidi)
* [ofx Beat Detection addon](https://github.com/darrenmothersele/ofxBeat)
* [easy cap](http://raspberry-at-home.com/video-grabber-for-raspberry-pi/)
* [Accesos Directos en Escritorio](http://www.raspberry-projects.com/pi/pi-operating-systems/raspbian/gui/desktop-shortcuts)
* [OF en Raspbian Stretch](https://forum.openframeworks.cc/t/compiling-of-in-raspbian-stretch/27562/42)
* [ofxPSBlend: Addon para blending](https://github.com/Akira-Hayasaka/ofxPSBlend)

## Ejemplo de Controlador MIDI custom con Arduino ##
* [Controlador MIDI Adafdruit con Botones](https://learn.adafruit.com/mini-untztrument-3d-printed-midi-controller/overview)

## REVISAR TOOLS
* [rasp Cam 01] (https://raspberrypi.stackexchange.com/questions/29717/raspberry-pi-recognizes-logitech-c920-webcam-but-cant-do-anything)
* [rasp Cam 02] (https://www.raspberrypi.org/forums/viewtopic.php?t=29879)
* [Link Camara Logitech] (https://www.logitech.com/es-mx/product/hd-pro-webcam-c920)
* [Configuración AudioInjector](http://www.flatmax.org/phpbb/viewtopic.php?f=5&t=3)
* [Referencia Modos Blend OpenGL](https://www.khronos.org/registry/OpenGL-Refpages/gl4/html/glBlendFunc.xhtml)

## openFrameworks Video Transparency
* [Problem with video transparency / alpha channel](https://forum.openframeworks.cc/t/problem-with-video-transparency-alpha-channel/18560)
* [Realtime video compositing](https://forum.openframeworks.cc/t/realtime-video-compositing/17898)
* [OpenGL add blend mode on frame buffer](https://forum.openframeworks.cc/t/opengl-add-blend-mode-on-frame-buffer/15315)
* [Opacity and blending FBOs problem](https://forum.openframeworks.cc/t/opacity-and-blending-fbos-problem/23408/9)
* [blending multiple FBOs with a shader](https://forum.openframeworks.cc/t/blending-multiple-fbos-with-a-shader/4302)
* [FBO Reference](https://www.songho.ca/opengl/gl_fbo.html)
* [SOLVED FBO and transparency](https://forum.openframeworks.cc/t/solved-fbo-and-transparency/18835)
* [FBO problems with alpha](https://forum.openframeworks.cc/t/fbo-problems-with-alpha/1643/12)
* [ofxInkSim: ink deposition simulator](https://forum.openframeworks.cc/t/ofxinksim-ink-deposition-simulator/19611)

## Update y comandos para la raspberry
* [git clone single branch](https://stackoverflow.com/questions/4811434/clone-only-one-branch?utm_medium=organic&utm_source=google_rich_qa&utm_campaign=google_rich_qa)
* Agregar comandos para realizar al inicio de la sesión en `.bashrc`
* Comando para evitar que la Raspberry se duerma `sh -c 'setterm -blank 0 -powersave off -powerdown 0 < /dev/console > /dev/console 2>&1'`
* Comando para hacer default la entrada RCA del audioInjector `alsactl --file /usr/share/doc/audioInjector/asound.state.RCA.thru.test restore`

## ToDo List
- Terminar presupuesto con dos versiones HARDWARE (NUC / raspberry)
- Camara. Probar funcionamiento con Rasp
// DEFINIR CAMARA
- Probar tarjeta de audio (mono o lo que sea)

- Crear Fbo contenedor de graficos (mapeable)
- Mascarilla con funcion numeroNodos
- Escena de debug
- Escena de mapeo
- Posible escena de inicio / stand by
- Guardar coordenadas de mapeo
- 8 canales = 8 capas por escena
- Probar ofxPSBlend con fbo's y revisar compatibilidad con Raspberry
