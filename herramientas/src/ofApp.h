#pragma once

#include "ofMain.h"
#include "ofxMidi.h"
#include "ofxBeat.h"

class ofApp : public ofBaseApp, public ofxMidiListener {

    //////// BEAT DETECTION ////////
    ofxBeat beat;

    public:
		void setup();
		void update();
		void draw();
        void exit();

		void keyPressed(int key);
		void keyReleased(int key);
		void mouseMoved(int x, int y );
		void mouseDragged(int x, int y, int button);
		void mousePressed(int x, int y, int button);
		void mouseReleased(int x, int y, int button);
		void mouseEntered(int x, int y);
		void mouseExited(int x, int y);
		void windowResized(int w, int h);
		void dragEvent(ofDragInfo dragInfo);
		void gotMessage(ofMessage msg);

        //////// AUDIO INPUT ////////
        void audioReceived(float*, int, int);
        void audioIn(float * input, int bufferSize, int nChannels);

        ofSoundStream soundStream;

        vector <float> left;
        vector <float> right;
        vector <float> volHistory;

        int 	bufferCounter;
        int 	drawCounter;

        float smoothedVol;
        float scaledVol;

        //////// CAMERA ////////
        ofVideoGrabber vidGrabber;
        ofPixels videoWithAlpha;
        ofTexture videoTexture;
        int camWidth;
        int camHeight;

        //////// MIDI LISTENER ////////
        void newMidiMessage(ofxMidiMessage& eventArgs);
        stringstream text;

        ofxMidiIn midiIn;
        ofxMidiMessage midiMessage;

        //////// SCENES DISPLAY FLAGS ////////
        int controller[6] = {25, 0, 0, 0, 0, 0};

        //////// SCENES DISPLAY FLAGS ////////
        bool sc_flags[6] = {false, false, false, false, false, false};

        //////// SCENES ////////
        void scene_1();
        void scene_2();
        void scene_3();
        void scene_4();
        void scene_5();
        void scene_6();

};
