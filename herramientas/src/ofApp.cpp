#include "ofApp.h"

//--------------------------------------------------------------
void ofApp::setup(){
    ofSetVerticalSync(true);
    // ofSetLogLevel(OF_LOG_VERBOSE);

    //////// AUDIO INITALIZATION ////////

        // 0 output channels,
        // 2 input channels
        // 44100 samples per second
        // 256 samples per buffer
        // 4 num buffers (latency)

        soundStream.printDeviceList();

        // int bufferSize = 256;
        int bufferSize = beat.getBufferSize();

    	left.assign(bufferSize, 0.0);
    	right.assign(bufferSize, 0.0);
    	volHistory.assign(400, 0.0);

    	bufferCounter	= 0;
    	drawCounter		= 0;
    	smoothedVol     = 0.0;
    	scaledVol		= 0.0;

    	soundStream.setup(this, 0, 2, 44100, bufferSize, 4);
        // ofSoundStreamSetup(0, 1, this, 44100, beat.getBufferSize(), 4);

    //////// MIDI INITALIZATION ////////
        // print input ports to console
    	midiIn.listPorts();

    	// open port by number (you may need to change this)
    	midiIn.openPort(1);

    	// don't ignore sysex, timing, & active sense messages,
    	// these are ignored by default
    	midiIn.ignoreTypes(false, false, false);

    	// add ofApp as a listener
    	midiIn.addListener(this);

    	// print received messages to the console
    	midiIn.setVerbose(true);

    //////// CAMERA INITALIZATION ////////
        camWidth = 640;  // try to grab at this size.
        camHeight = 480;

        //we can now get back a list of devices.
        vector<ofVideoDevice> devices = vidGrabber.listDevices();

        for(int i = 0; i < devices.size(); i++){
            if(devices[i].bAvailable){
                ofLogNotice() << devices[i].id << ": " << devices[i].deviceName;
            }else{
                ofLogNotice() << devices[i].id << ": " << devices[i].deviceName << " - unavailable ";
            }
        }

        vidGrabber.setDeviceID(0);
        vidGrabber.setDesiredFrameRate(30);
        vidGrabber.initGrabber(camWidth, camHeight);

        videoWithAlpha.allocate(camWidth, camHeight, OF_PIXELS_RGBA);
        videoTexture.allocate(videoWithAlpha);

    //////// CANVAS SETUP ////////
        ofBackground(60);
}

//--------------------------------------------------------------
void ofApp::update(){

    //////// CAMERA UPDATE ////////
        vidGrabber.update();

        if(vidGrabber.isFrameNew()){
            ofPixels & pixels = vidGrabber.getPixels();
            pixels.setImageType(OF_IMAGE_COLOR_ALPHA);
            for(int i = 0; i < pixels.size(); i+=4){
                videoWithAlpha[i] = pixels[i]; // R
                videoWithAlpha[i+1] = pixels[i+1]; // G
                videoWithAlpha[i+2] = pixels[i+2]; // B
                videoWithAlpha[i+3] = controller[0]; // A
            }

            videoTexture.loadData(videoWithAlpha);
        }

    //////// AUDIO UPDATE ////////
        beat.update(ofGetElapsedTimeMillis());
}

//--------------------------------------------------------------
void ofApp::draw(){
    ofTranslate(ofGetWidth()/2, ofGetHeight()/2);

    // vidGrabber.draw(-camWidth, -camHeight/2);
    // videoTexture.draw(0, -camHeight/2, camWidth, camHeight);

    //////// DRAW CAMERA FEED ////////
    videoTexture.draw(-camWidth/2, -camHeight/2, camWidth, camHeight);

    ofNoFill();
    ofDrawCircle(0, 0, ofGetHeight()/2);

    //////// DRAW SCENES IF ENABLED ////////
    if(sc_flags[0]) scene_1();
    if(sc_flags[1]) scene_2();
    if(sc_flags[2]) scene_3();
    if(sc_flags[3]) scene_4();
    if(sc_flags[4]) scene_5();
    if(sc_flags[5]) scene_6();

    //////// BEAT DETECTION DATA ////////
    cout << beat.kick() << "," << beat.snare() << "," << beat.hihat() << endl;

}

//--------------------------------------------------------------
void ofApp::exit() {

	// clean up
	midiIn.closePort();
	midiIn.removeListener(this);
}

//--------------------------------------------------------------
void ofApp::newMidiMessage(ofxMidiMessage& msg) {

	// make a copy of the latest message
	midiMessage = msg;

    switch(midiMessage.control){
        //////// SCENES PADS ////////
        case 1 :
            sc_flags[0] = !sc_flags[0];
        break;
        case 2 :
            sc_flags[1] = !sc_flags[1];
        break;
        case 3 :
            sc_flags[2] = !sc_flags[2];
        break;
        case 4 :
            sc_flags[3] = !sc_flags[3];
        break;
        case 5 :
            sc_flags[4] = !sc_flags[4];
        break;
        case 6 :
            sc_flags[5] = !sc_flags[5];
        break;
        //////// CAMERA FADER ////////
        case 7 :
            controller[0] = msg.value;
        break;
        //////// PARAMETER CONTROLLERS ////////
        case 8 :
            controller[1] = msg.value;
        break;
        case 9 :
            controller[2] = msg.value;
        break;
        case 10 :
            controller[3] = msg.value;
        break;
        case 11 :
            controller[4] = msg.value;
        break;
        case 12 :
            controller[5] = msg.value;
        break;

    }

}

//--------------------------------------------------------------
void ofApp::keyPressed(int key){
    switch(key){
        case '1' :
            sc_flags[0] = !sc_flags[0];
        break;
        case '2' :
            sc_flags[1] = !sc_flags[1];
        break;
        case '3' :
            sc_flags[2] = !sc_flags[2];
        break;
        case '4' :
            sc_flags[3] = !sc_flags[3];
        break;
        case '5' :
            sc_flags[4] = !sc_flags[4];
        break;
        case '6' :
            sc_flags[5] = !sc_flags[5];
        break;
        case 'l':
			midiIn.listPorts();
		break;

    }

}

//--------------------------------------------------------------
void ofApp::scene_1(){
    cout << "scene1" << endl;
    ofSetColor(255, 0, 0, 200);
    ofFill();
    ofDrawCircle(0, 0, ofGetHeight()/4);
};

//--------------------------------------------------------------
void ofApp::scene_2(){
    cout << "scene2" << endl;
    ofSetColor(0, 255, 0, 200);
    ofFill();
    ofDrawCircle(0, 0, ofGetHeight()/4);

};
//--------------------------------------------------------------
void ofApp::scene_3(){
    cout << "scene3" << endl;
    ofSetColor(0, 0, 255, 200);
    ofFill();
    ofDrawCircle(0, 0, ofGetHeight()/4);

};

//--------------------------------------------------------------
void ofApp::scene_4(){
    cout << "scene4" << endl;
    ofSetColor(255, 255, 255, 200);
    ofFill();
    ofDrawCircle(0, 0, ofGetHeight()/4);

};
//--------------------------------------------------------------
void ofApp::scene_5(){
    cout << "scene5" << endl;
    ofSetColor(0, 0, 0, 200);
    ofFill();
    ofDrawCircle(0, 0, ofGetHeight()/4);

};

//--------------------------------------------------------------
void ofApp::scene_6(){
    cout << "scene6" << endl;
    ofSetColor(120, 0, 80, 200);
    ofFill();
    ofDrawCircle(0, 0, ofGetHeight()/4);

};

//--------------------------------------------------------------
void ofApp::audioReceived(float* input, int bufferSize, int nChannels) {
  beat.audioReceived(input, bufferSize, nChannels);
}

//--------------------------------------------------------------
void ofApp::audioIn(float * input, int bufferSize, int nChannels){

	float curVol = 0.0;

	// samples are "interleaved"
	int numCounted = 0;

	//lets go through each sample and calculate the root mean square which is a rough way to calculate volume
	for (int i = 0; i < bufferSize; i++){
		left[i]		= input[i*2]*0.5;
		right[i]	= input[i*2+1]*0.5;

		curVol += left[i] * left[i];
		curVol += right[i] * right[i];
		numCounted+=2;
	}

	//this is how we get the mean of rms :)
	curVol /= (float)numCounted;

	// this is how we get the root of rms :)
	curVol = sqrt( curVol );

	smoothedVol *= 0.93;
	smoothedVol += 0.07 * curVol;

	bufferCounter++;

}

//--------------------------------------------------------------
void ofApp::keyReleased(int key){

}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y ){

}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseEntered(int x, int y){

}

//--------------------------------------------------------------
void ofApp::mouseExited(int x, int y){

}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h){

}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo){

}
