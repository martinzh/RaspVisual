#pragma once

#include "ofMain.h"
#include "ofxMidi.h"
#include "ofxBeat.h"
#include <cmath>

#define NFBO 10
#define NCONT 9

class ofApp : public ofBaseApp, public ofxMidiListener{

    //////// BEAT DETECTION ////////
    ofxBeat beat;

	public:
		void setup();
		void update();
		void draw();
        void exit();

		void keyPressed(int key);
		void keyReleased(int key);
		void mouseMoved(int x, int y );
		void mouseDragged(int x, int y, int button);
		void mousePressed(int x, int y, int button);
		void mouseReleased(int x, int y, int button);
		void mouseEntered(int x, int y);
		void mouseExited(int x, int y);
		void windowResized(int w, int h);
		void dragEvent(ofDragInfo dragInfo);
		void gotMessage(ofMessage msg);

        //////// AUDIO INPUT ////////
        void audioReceived(float*, int, int);
        // void audioIn(float * input, int bufferSize, int nChannels);

        ofSoundStream soundStream;

        //////// MIDI LISTENER ////////
        void newMidiMessage(ofxMidiMessage& eventArgs);
        stringstream text;

        ofxMidiIn midiIn;
        ofxMidiMessage midiMessage;

        //////// SCENES DISPLAY FLAGS ////////
        bool sc_flags[NFBO];

        //////// SCENES DISPLAY FLAGS ////////
        int controller[NCONT];

        //////// FBO INSTANCES ////////
        ofFbo fbo[NFBO];

        //////// FBO SCENES FUNCTIONS ////////
        void moon();
        void circs();
        void spiral();
        void rays();
        void figCirc();
        void spectrum();
        void confetti();
        void lines();
        void tunel();
        void tentacles();

};
