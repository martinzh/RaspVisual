#include "ofApp.h"

float r;
float sx;
float sy;
int k;
double R;

//--------------------------------------------------------------
void ofApp::setup(){

    ofSetVerticalSync(true);
    ofSetCircleResolution(80);

    //////// AUDIO INITALIZATION ////////

    // 0 output channels,
    // 2 input channels
    // 44100 samples per second
    // 256 samples per buffer
    // 4 num buffers (latency)

    soundStream.printDeviceList();
    // soundStream.setDeviceID(3);

    int bufferSize = beat.getBufferSize();
    soundStream.setup(this, 0, 1, 44100, bufferSize, 4);

    //////// MIDI INITALIZATION ////////

    // print input ports to console
    midiIn.listPorts();

    // open port by number (you may need to change this)
    midiIn.openPort(0);

    // don't ignore sysex, timing, & active sense messages,
    // these are ignored by default
    midiIn.ignoreTypes(false, false, false);

    // add ofApp as a listener
    midiIn.addListener(this);

    // print received messages to the console
    midiIn.setVerbose(true);

    /////////// GLOBAL VARIABLES ///////////

    r = 25;
    sx = 50;
    sy = 50;
    R = ofGetHeight()/2;

    /////////// SCENE FLAGS ///////////

    for (size_t i = 0; i < NFBO; i++) {
        sc_flags[i] = false;
    }

    /////////// CONTROLLER ARRAY ///////////

    for (size_t i = 0; i < NCONT; i++) {
        controller[i] = 0;
    }

    /////////// INITIALIZE FBO INSTANCES ///////////

    for (size_t i = 0; i < NFBO; i++) {
        fbo[i].allocate(ofGetWidth(), ofGetHeight());
        fbo[i].begin();
        ofClear(255,255,255);
        fbo[i].end();
    }

    //////// CANVAS SETUP ////////

    ofBackground(25);
}

//--------------------------------------------------------------
void ofApp::update(){
    beat.update(ofGetElapsedTimeMillis());

    moon();
    circs();
    spiral();
    rays();
    figCirc();
    spectrum();
    confetti();
    lines();
    tunel();
    tentacles();
}

//--------------------------------------------------------------
void ofApp::draw(){

    for (size_t i = 0; i < NFBO; i++) {
        if(sc_flags[i]) fbo[i].draw(0,0);
    }

    // fbo[0].draw(0, 0);
    // fbo[1].draw(0, 0);
    // fbo[2].draw(0, 0);
    // fbo[3].draw(0, 0);
    // fbo[4].draw(0, 0);
    // fbo[5].draw(0, 0);
    // fbo[6].draw(0, 0);
    // fbo[7].draw(0, 0);
    // fbo[8].draw(0, 0);
    // fbo[9].draw(0, 0);

    // if(sc_flags[0]) scene_1();
    // if(sc_flags[1]) scene_2();
    // if(sc_flags[2]) scene_3();
    // if(sc_flags[3]) scene_4();
    // if(sc_flags[4]) scene_5();
    // if(sc_flags[5]) scene_6();
    // if(sc_flags[6]) scene_6();
    // if(sc_flags[7]) scene_6();
    // if(sc_flags[8]) scene_6();
    // if(sc_flags[9]) scene_6();

}

//--------------------------------------------------------------
void ofApp::exit() {

	// clean up
	midiIn.closePort();
	midiIn.removeListener(this);
}

//--------------------------------------------------------------
void ofApp::audioReceived(float* input, int bufferSize, int nChannels) {
    beat.audioReceived(input, bufferSize, nChannels);
}

//--------------------------------------------------------------
void ofApp::newMidiMessage(ofxMidiMessage& msg) {

	// make a copy of the latest message
	midiMessage = msg;

    switch(midiMessage.control){
        //////// SCENES PADS ////////
        case 0 :
            sc_flags[0] = !sc_flags[0];
        break;
        case 1 :
            sc_flags[1] = !sc_flags[1];
        break;
        case 2 :
            sc_flags[2] = !sc_flags[2];
        break;
        case 3 :
            sc_flags[3] = !sc_flags[3];
        break;
        case 4 :
            sc_flags[4] = !sc_flags[4];
        break;
        case 5 :
            sc_flags[5] = !sc_flags[5];
        break;
        case 6 :
            sc_flags[6] = !sc_flags[6];
        break;
        case 7 :
            sc_flags[7] = !sc_flags[7];
        break;
        case 8 :
            sc_flags[8] = !sc_flags[8];
        break;
        case 9 :
            sc_flags[9] = !sc_flags[9];
        break;
        //////// CAMERA FADER ////////
        case 28 :
            controller[0] = msg.value;
        break;
        //////// X SCREEN ADJUST ////////
        case 16 :
            controller[1] = msg.value;
        break;
        //////// Y SCREEN ADJUST ////////
        case 17 :
            controller[2] = msg.value;
        break;
        //////// MASTER ALPHA ////////
        case 29 :
            controller[3] = msg.value;
        break;
        //////// PARAMETER CONTROLLERS ////////
        case 30 :
            controller[4] = msg.value;
        break;
        case 31 :
            controller[5] = msg.value;
        break;
        case 32 :
            controller[6] = msg.value;
        break;
        case 33 :
            controller[7] = msg.value;
        break;
        case 34 :
            controller[8] = msg.value;
        break;

    }

}

//--------------------------------------------------------------
void ofApp::moon(){

    fbo[0].begin();

    ofPushMatrix();

    ofSetColor(0,  25);
    ofDrawRectangle(0,0,ofGetWidth(),ofGetHeight());

    ofTranslate(ofGetWidth()/2, ofGetHeight()/2);

    ofSetColor(255, ofMap(beat.getBand(1), 0.2, 1.5, 50, 200, true));

    if(beat.getBand(5) >= 0.4){
        for (size_t i = 0; i < 100; i++) {

            float ang = ofRandom(TWO_PI);
            float ang1 = ofRandom(TWO_PI);

            ofDrawLine(R*cos(ang), R*sin(ang), R*cos(ang1), R*sin(ang1));
        }
    }
    ofPopMatrix();

    fbo[0].end();
}

//--------------------------------------------------------------
void ofApp::circs(){

    int rad = ofGetHeight()/2 / 12;
    float n_circs = 22.f;
    float ang = TWO_PI / n_circs;
    float scale;

    fbo[1].begin();

    ofSetColor(0,  25);
    ofDrawRectangle(0,0,ofGetWidth(),ofGetHeight());

    ofTranslate(ofGetWidth()/2, ofGetHeight()/2);

    ofNoFill();
    ofSetColor(255, ofMap(beat.getBand(1), 0.2, 1.5, 90, 250, true));

    for (size_t i = 1; i < 12; i++) {
        ofPushMatrix();
        scale = ofMap(beat.kick(), 0.0, 1.0, 0.5, 1., true);

        if(beat.getBand(5) >= 0.5) ofRotate(ofMap(beat.getBand(5), 0.5, 1.0, i + ofRandom(-10,10), i + ofRandom(-2,2), true));
        if(beat.getBand(1) >= 0.8) ofScale(scale, scale, scale);

        for (size_t j = 0; j < n_circs; j++) {
            ofDrawCircle(i * rad * cos(j * ang), i * rad * sin(j * ang), i*i* sin(ofGetElapsedTimeMillis()/500));
        }
        ofPopMatrix();
    }

    fbo[1].end();
}

//--------------------------------------------------------------
void ofApp::spiral(){

    int rad = ofGetHeight()/2 / 25;
    float n_circs = 20.f;
    float ang = TWO_PI / n_circs;
    float alpha;
    float scale;

    fbo[2].begin();

    ofSetColor(0,  25);
    ofDrawRectangle(0,0,ofGetWidth(),ofGetHeight());

    ofSetColor(255, ofMap(beat.kick(), 0.0, 1.0, 0, 80));
    ofDrawRectangle(0,0,ofGetWidth(),ofGetHeight());

    ofTranslate(ofGetWidth()/2, ofGetHeight()/2);

    // ofNoFill();
    // alpha = ofMap(beat.hihat(), 0.0, 1.0, 50, 220, true);
    // alpha = ofMap(beat.kick(), 0.0, 1.0, 50, 250, true);
    alpha = ofMap(beat.getBand(2), 0.3, 1.2, 50, 250, true);
    // scale = ofMap(beat.kick(), 0.0, 1.0, 0.5, 1.2, true);

    // alpha = ofMap(beat.getBand(5), 0.2, 1.5, 1, 200, true);
    // scale = ofMap(beat.getBand(6), 0.1, 1.3, 0.1, 1.1, true);

    scale = ofMap(beat.getBand(5), 0.3, 1.0, 0.65, 1.0f, true);

    for (size_t i = 1; i < 25; i++) {

        ofSetColor(255 * (i % 2), i + alpha );

        ofPushMatrix();

        // if(beat.getBand(4) >= 0.3) ofRotate( 2 * i * sin(ofGetElapsedTimef()));
        if(beat.getBand(4) >= 0.3) ofRotate( 2 * i * ofGetElapsedTimef());
        // ofRotate( 10*i + 10 * ofGetElapsedTimef());

        scale = fmod( i + ofGetElapsedTimef()/2.2, 1.1);

        ofScale(scale, scale, scale);

        for (int j = 0; j < n_circs; j++) {

            ofDrawCircle(i * rad * cos(j * ang), i * rad * sin(j * ang), 2  * i);
            ofDrawCircle(i * rad * sin(j * ang), i * rad * cos(j * ang), 2  * i);

            ofDrawCircle(i * rad * cos(-j * ang), i * rad * sin(-j * ang), 2  * i);
            ofDrawCircle(i * rad * sin(-j * ang), i * rad * cos(-j * ang), 2  * i);
        }

        ofPopMatrix();
    }

    fbo[2].end();
}

//--------------------------------------------------------------
void ofApp::figCirc(){

    float ang = ofGetElapsedTimef();

    float alpha;
    float scale;

    fbo[3].begin();

    ofSetColor(0,  1);
    ofDrawRectangle(0,0,ofGetWidth(),ofGetHeight());

    ofTranslate(ofGetWidth()/2, ofGetHeight()/2);

    ofSetColor(255, 200);

    ofDrawLine(r*cos(2*ang), r*sin(2*ang), R*cos(ang), R*sin(ang));

    // for (size_t i = 1; i < 15; i++) {
        // ofDrawLine(i*r*cos(i*ang), i*r*sin(i*ang), (i+1)*r*cos((i+1)*ang), (i+1)*r*sin((i+1)*ang));
        // ofDrawLine(i*r*cos(-i*ang), i*r*sin(-i*ang), (i+1)*r*cos(-(i+1)*ang), (i+1)*r*sin((-i+1)*ang));
    // }

    fbo[3].end();
}

//--------------------------------------------------------------

void ofApp::rays(){

    float n_circs = 100.f;
    float ang = TWO_PI / n_circs;
    float offSet = 100;
    float r = ofGetHeight()/8;
    float R = ofGetHeight()/4;

    float alpha;
    float scale;

    fbo[4].begin();

    ofSetColor(0,  15);
    ofDrawRectangle(0,0,ofGetWidth(),ofGetHeight());

    ofTranslate(ofGetWidth()/2, ofGetHeight()/2);

    ofRotate( 10 * ofGetElapsedTimef());

    ofSetColor(255, 200);

    for (int i = 0; i < n_circs; i++) {
        ofDrawLine(r*cos(i*ang), r*sin(i*ang), (R + ofMap(beat.getBand(i % 16), 0.0, 1.2, -offSet, offSet))*cos(i*ang), (R + ofMap(beat.getBand(i % 16), 0.0, 1.2, -offSet, offSet))*sin(i*ang));
    }

    fbo[4].end();
}

//--------------------------------------------------------------
void ofApp::spectrum(){

    float sx = ofGetHeight()/2 / 16;
    float bandVal;
    float rectW;
    float rad;

    fbo[5].begin();

    ofSetColor(0,  25);
    ofDrawRectangle(0,0,ofGetWidth(),ofGetHeight());

    ofPushMatrix();

    ofTranslate(ofGetWidth()/2, ofGetHeight()/2);

    ofNoFill();
    ofSetColor(255, 200);

    rad = fmod(ofGetElapsedTimeMillis(), float(ofGetHeight()/2));

    ofSetLineWidth(2);
    if( beat.kick() > 0.0) ofDrawCircle(0, 0, rad, rad);

    rectW = ofMap(beat.kick(), 0.0, 1.0, 5, 15);

    ofSetRectMode(OF_RECTMODE_CENTER);

    ofFill();
    ofSetLineWidth(1);

    for (int i = 0; i < 15; i++) {

        bandVal = ofMap(beat.getBand(i), 0.0, 1.5, 0.0, ofGetHeight()/2);

        ofDrawRectangle(i*sx, 0,rectW,  bandVal);
        ofDrawRectangle(-i*sx, 0,rectW,  bandVal);

        // ofDrawRectangle(i*sx, 0, rectW, -bandVal);
        // ofDrawRectangle(-i*sx, 0, rectW, -bandVal);
    }

    ofSetRectMode(OF_RECTMODE_CORNER);

    ofPopMatrix();

    fbo[5].end();
}

//--------------------------------------------------------------

void ofApp::confetti(){

    float r;
    float ang;
    float rad;

    fbo[6].begin();

    ofSetColor(0,  25);
    ofDrawRectangle(0,0,ofGetWidth(),ofGetHeight());

    ofPushMatrix();

    ofTranslate(ofGetWidth()/2, ofGetHeight()/2);

    rad = fmod(25*ofGetElapsedTimef(), float(ofGetHeight()/2));

    ofNoFill();
    ofSetColor(255, ofMap(beat.getBand(6), 0.2, 1.2, 0, 220));
    ofDrawCircle(0,0, rad + ofMap(beat.getBand(8), 0.0, 1.0, -20, 20));

    ofFill();
    ofSetColor(255, ofMap(beat.getBand(5), 0.2, 1.2, 50, 250));


    if(beat.kick() > 0.){
        for (int i = 0; i < 50; i++) {

            r = ofRandom(5, ofGetHeight()/2);
            ang = ofRandom(TWO_PI);

            ofDrawCircle(r*cos(ang), r*sin(ang), ofRandom(2, 25));
        }
    }

    ofPopMatrix();

    fbo[6].end();
}

//--------------------------------------------------------------

void ofApp::lines(){

    int n_r = 16;
    float sy = (ofGetHeight()/2) / float(n_r);
    float bandVal;
    float hBandVal;
    float rel = fmod(ofGetElapsedTimeMillis(), 0.1);
    ofColor c = ofColor(0);

    fbo[7].begin();

    ofSetColor(0,  25);

    ofDrawRectangle(0,0,ofGetWidth(),ofGetHeight());

    ofPushMatrix();

    ofTranslate(ofGetWidth()/2, ofGetHeight()/2);

    ofFill();
    ofSetRectMode(OF_RECTMODE_CENTER);

    // ofSetColor( 255, 0, 0, ofMap(beat.kick(), 0.0, 1.0, 0, 200));
    c.setHsb(ofMap(beat.getBand(1), 0.0, 1.0, 120, 180), 255, 255);
    ofSetColor( c, ofMap(beat.kick(), 0.0, 1.0, 10, 200));

    ofDrawCircle(0,0,ofGetHeight()/2, ofGetHeight()/2);

    for (int i = 0; i < n_r; i++) {

        bandVal = ofMap(beat.getBand(i)- rel, 0.0, 1.5, 0.0, ofGetHeight()/2) ;
        hBandVal = ofMap(beat.getBand(i)- rel, 0.0, 1.5, 1, 50) ;

        ofSetColor( 5 * 25*(i % 4), ofMap(beat.getBand(i), 0.2, 1.2, 50, 200));

        ofDrawRectRounded(0, -i*sy, bandVal, hBandVal, 10);
        ofDrawRectRounded(0, i*sy, bandVal, hBandVal, 10);
    }

    ofSetRectMode(OF_RECTMODE_CORNER);
    ofPopMatrix();

    fbo[7].end();
}

//--------------------------------------------------------------

void ofApp::tunel(){

    int n_r = 24;
    float o_x = 0.0;
    float o_y = 0.0;
    float v_i;

    float s_r = ofGetHeight()/2 / float(n_r);
    ofColor c = ofColor(0);

    fbo[8].begin();

    ofSetColor(0,  25);

    ofDrawRectangle(0,0,ofGetWidth(),ofGetHeight());

    ofPushMatrix();

    ofTranslate(ofGetWidth()/2, ofGetHeight()/2);

    ofFill();
    // ofSetRectMode(OF_RECTMODE_CENTER);

    // ofSetColor( 255, 0, 0, ofMap(beat.kick(), 0.0, 1.0, 0, 200));
    // c.setHsb(ofMap(beat.getBand(1), 0.0, 1.0, 120, 180), 255, 255);
    // ofSetColor( c, ofMap(beat.kick(), 0.0, 1.0, 10, 200));

    // ofDrawCircle(0,0,ofGetHeight()/2, ofGetHeight()/2);

    // o_r = ofMap(beat.kick(), 0.0, 1.0, -3, 3);
    // if(beat.getBand(1) > 0.6) o_r = ofRandom(-1.0,1.0);

    for (int i = n_r; i > 0; i--) {

        if(beat.getBand(i % 16) > 0.4){
            // o_x = ofNoise(i*ofGetElapsedTimef());
            // o_y = ofNoise(i*ofGetElapsedTimef());
            // o_x = ofMap(beat.getBand(i % 16), 0.4, 1.0, -10, 10) * (-1.0 + ofNoise(i*ofGetElapsedTimef()));
            // o_y = ofMap(beat.getBand(i % 16), 0.4, 1.0, -10, 10) * (-1.0 + ofNoise(i*ofGetElapsedTimef()));
            o_x = ofMap(beat.getBand(i % 16), 0.4, 1.0, -10, 10) ;
            o_y = ofMap(beat.getBand(i % 16), 0.4, 1.0, -10, 10) ;
        }

        v_i = ofMap(i, 0, n_r, 5, 1);

        // ofSetColor( 5 * 25*(i % 4), ofMap(beat.getBand(i), 0.2, 1.2, 50, 200));
        ofSetColor( 5 * 12*(i % 8), 255);

        // ofDrawCircle(i*cos(ofGetElapsedTimef() + o_r), i*sin(ofGetElapsedTimef() + o_r), 2*i + s_r, 2*i + s_r);
        // ofDrawCircle(-i*cos(ofGetElapsedTimef() + o_r), -i*sin(ofGetElapsedTimef() + o_r), 2*i + s_r, 2*i + s_r);

        // ofDrawCircle(i*cos(i + ofGetElapsedTimef()) + i*o_r, i*sin(i + ofGetElapsedTimef()) + i*o_r, s_r*i, s_r*i);
        // ofDrawCircle((n_r - 1 + s_r * i)*cos(i + ofGetElapsedTimef()) + o_r, (n_r - 1 + s_r * i)*sin(i + ofGetElapsedTimef()) + o_r, s_r*i, s_r*i);

        // ofDrawCircle(i*cos(v_i*ofGetElapsedTimef()) + o_x, i*sin(v_i*ofGetElapsedTimef())+ o_y, s_r*i, s_r*i);
        ofDrawCircle(o_x, o_y, s_r*i, s_r*i);

    }

    ofSetRectMode(OF_RECTMODE_CORNER);
    ofPopMatrix();

    fbo[8].end();
}

//--------------------------------------------------------------

void ofApp::tentacles(){

    int n_r = 24;
    float o_x = 0.0;
    float o_y = 0.0;
    float v_i;
    int arms = 8;
    float ang = TWO_PI / float(arms);
    float scale = 2.5;

    float s_r = ofGetHeight()/2 / float(n_r);
    ofColor c = ofColor(0);

    fbo[9].begin();

    ofSetColor(0,  15);

    ofDrawRectangle(0,0,ofGetWidth(),ofGetHeight());

    ofTranslate(ofGetWidth()/2, ofGetHeight()/2);

    ofFill();

    c.setHsb(ofMap(beat.getBand(12), 0.0, 1.0, 120, 180), 255, 255);
    ofSetColor( c, ofMap(beat.hihat(), 0.0, 1.0, 10, 200));

    ofDrawCircle(0,0,ofGetHeight()/2, ofGetHeight()/2);


    for (int j = 0; j < arms; j++) {


        for (int i = n_r; i > 0; i--) {
            ofPushMatrix();

            // scale = ofMap(beat.getBand(i % 16), 0.2, 1.2, -10, 10);
            scale = ofMap(beat.getBand(i % 16), 0.2, 1.2, -0.25, 0.25);

            ofScale(0.8 + scale, 0.8 + scale, 0.8 + scale);

            v_i = ofMap(i, 0, n_r, 1.5, 0.05);

            ofSetColor( 5 * 12*(i % 8), 255);

            ofDrawCircle((n_r - i)*s_r*cos(j*ang) * cos(v_i * ofGetElapsedTimef()), (n_r - i)*s_r*sin(j*ang) * sin(v_i * ofGetElapsedTimef()), 2.5*i + scale, 2.5*i + scale);
            ofDrawCircle(-(n_r - i)*s_r*cos(j*ang) * cos(v_i * ofGetElapsedTimef()), -(n_r - i)*s_r*sin(j*ang) * sin(v_i * ofGetElapsedTimef()), 2.5*i + scale, 2.5*i + scale);

            // ofDrawCircle((n_r - i)*s_r*cos(j*ang) * cos(v_i * ofGetElapsedTimef()), (n_r - i)*s_r*sin(j*ang) * sin(v_i * ofGetElapsedTimef()), scale*i, scale*i);
            // ofDrawCircle(-(n_r - i)*s_r*cos(j*ang) * cos(v_i * ofGetElapsedTimef()), -(n_r - i)*s_r*sin(j*ang) * sin(v_i * ofGetElapsedTimef()), scale*i, scale*i);
            ofPopMatrix();
        }
    }


    fbo[9].end();
}


//--------------------------------------------------------------
void ofApp::keyPressed(int key){
    switch(key){
        case '0' :
            sc_flags[0] = !sc_flags[0];
        break;
        case '1' :
            sc_flags[1] = !sc_flags[1];
        break;
        case '2' :
            sc_flags[2] = !sc_flags[2];
        break;
        case '3' :
            sc_flags[3] = !sc_flags[3];
        break;
        case '4' :
            sc_flags[4] = !sc_flags[4];
        break;
        case '5' :
            sc_flags[5] = !sc_flags[5];
        break;
        case '6' :
            sc_flags[6] = !sc_flags[6];
        break;
        case '7' :
            sc_flags[7] = !sc_flags[7];
        break;
        case '8' :
            sc_flags[8] = !sc_flags[8];
        break;
        case '9' :
            sc_flags[9] = !sc_flags[9];
        break;
        case 'l':
            midiIn.listPorts();
        break;

    }
}

//--------------------------------------------------------------
void ofApp::keyReleased(int key){

}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y ){

}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button){
}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseEntered(int x, int y){

}

//--------------------------------------------------------------
void ofApp::mouseExited(int x, int y){

}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h){

}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo){

}
