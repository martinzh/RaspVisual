#include "ofApp.h"

float r;
float sx;
float sy;
int k;
double R;

//////////////////////////
int centroX, centroY;	// centro de ventana
int radioMasc;			// radio de la mascarilla
int numSegMasc;			// numero de segmentos de la mascarilla (minimo 3)

bool debug = true;
bool mapeaTeclas = false;
bool mapeaMidi = false;
bool help = false;

int ancho, alto;		// Ancho y alto de ventana.
float escalaVideo = 2.0f;
int numEscenasCam = 5; // Numero de escenas de cámara
int escenaCamAct = 0; // Escena actual de la cámara
int opaVideo;
int sep = 20;

/// OTRAS ESCENAS DE GRAFICA
vector<float> volY;
int totalVols = 2;
int activaContenido, numCont = 3;
int velRot = 10;
int opa = 0;

ofVec2f coordFbo[4];	// Coordenadas para mapeo de Fbo
ofVec2f posMap;

ofColor globalColor;

//--------------------------------------------------------------
void ofApp::setup() {
	ofSetVerticalSync(true);
	ofSetCircleResolution(80);

	//////// AUDIO INITALIZATION ////////

	// 0 output channels,
	// 2 input channels
	// 44100 samples per second
	// 256 samples per buffer
	// 4 num buffers (latency)
	soundStream.printDeviceList();
	soundStream.setDeviceID(0);

	int bufferSize = beat.getBufferSize();
	soundStream.setup(this, 0, 1, 44100, bufferSize, 4);

	//////// MIDI INITALIZATION ////////

	// print input ports to console
	midiIn.listPorts();

	// open port by number (you may need to change this)
	midiIn.openPort(0);

	// don't ignore sysex, timing, & active sense messages,
	// these are ignored by default
	midiIn.ignoreTypes(false, false, false);

	// add ofApp as a listener
	midiIn.addListener(this);

	// print received messages to the console
	midiIn.setVerbose(true);

    //////// CAMERA INITALIZATION ////////
	if (camaraON) {
		camWidth = 640;  // try to grab at this size.
		camHeight = 480;
		vector<ofVideoDevice> devices = vidGrabber.listDevices();

		for (int i = 0; i < devices.size(); i++) {
			if (devices[i].bAvailable) {
				ofLogNotice() << devices[i].id << ": " << devices[i].deviceName;
			}
			else {
				ofLogNotice() << devices[i].id << ": " << devices[i].deviceName << " - unavailable ";
			}
		}

		vidGrabber.setDeviceID(0);
		vidGrabber.setDesiredFrameRate(30);
		vidGrabber.initGrabber(camWidth, camHeight);

		videoWithAlpha.allocate(camWidth, camHeight, OF_PIXELS_RGBA);
		videoTexture.allocate(videoWithAlpha);
		///  FBO
		fboCam.allocate(ofGetWidth(), ofGetHeight());
		fboCam.begin();
		ofClear(0, 0, 0);
		fboCam.end();
	}

	/////////// GLOBAL VARIABLES ///////////
	r = 25;
	sx = 50;
	sy = 50;
	R = ofGetHeight() / 3;

	/////////// SCENE FLAGS ///////////

	for (size_t i = 0; i < NFBO; i++) {
		sc_flags[i] = false;
        sceneColor[i] = ofColor(0);
        sceneColor[i].setHsb(0,0,255,255);
	}

    globalColor = ofColor(0);
    globalColor.setHsb(0,0,255,255);

	//    curr_FBO = ofRandom(NFBO);
	//    next_FBO = ofRandom(NFBO);

	//    sc_flags[curr_FBO] = true;

    transInter = 5.0;

	/////////// CONTROLLER ARRAY ///////////

	for (size_t i = 0; i < NCONT; i++) {
		controller[i] = 0;
	}

    controller[3] = 60;

	/////////// INITIALIZE FBO INSTANCES ///////////
	for (size_t i = 0; i < NFBO; i++) {
		fbo[i].allocate(ofGetWidth(), ofGetHeight());
		fbo[i].begin();
		ofClear(0, 0, 0);
		fbo[i].end();
	}
	//////// CANVAS SETUP ////////

	ofBackground(0);

	//////// FBO MASCARILLA ////////
	fboMaster.allocate(ofGetWidth(), ofGetHeight(), GL_RGBA);
	fboHelp.allocate(ofGetWidth()*.5, ofGetHeight()*.8, GL_RGBA);
	fboGenMask.allocate(ofGetWidth(), ofGetHeight(), GL_RGBA);

	// Vectores para mapeo			// indagar como mapear, usar fbo como textura en beginShape - endShape
	centroX = ofGetWidth() / 2;
	centroY = ofGetHeight() / 2;
	ancho = ofGetWidth();
	alto = ofGetHeight();

	// Vectores para mapeo			// indagar como mapear, usar fbo como textura en beginShape - endShape
	coordFbo[0] = ofVec2f(0, 0);
	coordFbo[1] = ofVec2f(ancho, 0);
	coordFbo[2] = ofVec2f(ancho, alto);
	coordFbo[3] = ofVec2f(0, alto);

	posMap = ofVec2f(0,0);			// posicion de contenidos (se ubica a partir del centro)

	radioMasc = int(alto*.45);
	numSegMasc = 60;

	/// OTRAS ESCENAS DE GRAFICA
	volY.resize(totalVols);
	for (int i = 0; i < totalVols; i++) {
		volY.push_back(0);
	}
}

//--------------------------------------------------------------
void ofApp::update() {
    beat.update(ofGetElapsedTimeMillis());

    //////// CAMERA UPDATE ////////
	if (camaraON) {
		vidGrabber.update();
		if (vidGrabber.isFrameNew()) {
			ofPixels & pixels = vidGrabber.getPixels();
			pixels.setImageType(OF_IMAGE_COLOR_ALPHA);

			switch (escenaCamAct) {
			case 0:
				for (int i = 0; i < pixels.size(); i += 4) {
					videoWithAlpha[i] = pixels[i]; // R
					videoWithAlpha[i + 1] = pixels[i + 1]; // G
					videoWithAlpha[i + 2] = pixels[i + 2]; // B
					videoWithAlpha[i + 3] = controller[0]; // A
				}
				videoTexture.loadData(videoWithAlpha);
				break;
			}
		}
	}

    if(mapeaMidi){
        posMap.x = ofMap(controller[1], 0, 127, 0, ofGetWidth(), true) - centroX;
        posMap.y = ofMap(controller[2], 0, 127, 0, ofGetHeight(), true) - centroY;
    }

    mascarilla();

    // next_FBO = ofRandom(NFBO);
    // sc_flags[next_FBO] = !sc_flags[next_FBO];

    // printf("%d\t%d\n", curr_FBO, next_FBO);
    //printf("%g\n", ofGetElapsedTimef());

    transInter = ofMap(controller[5], 0, 127, 1.0, 10.0, true);

    // if (fmod(ofGetElapsedTimef() - 0.5*transInter, transInter) >= 0.0 ){
    //
    //     sc_flags[next_FBO] = !sc_flags[next_FBO];
    //
    //     next_FBO = ofRandom(NFBO);
    //
    //     sc_flags[next_FBO] = !sc_flags[next_FBO];
    //     printf("half\n");
    // }

    // if (fmod(ofGetElapsedTimef(), transInter) >= 0.0 ){
    //if (ofGetElapsedTimef() >= transInter){

      //  sc_flags[curr_FBO] = !sc_flags[curr_FBO];

        //curr_FBO = ofRandom(NFBO);

        //sc_flags[curr_FBO] = !sc_flags[curr_FBO];
        //printf("full\n");

        // printf("%d\t%d\n", curr_FBO, next_FBO);
        //ofResetElapsedTimeCounter();
    //}

    // if (ofGetElapsedTimef() >= 2*transInter) ofResetElapsedTimeCounter();

	if (sc_flags[0]) moon();
	if (sc_flags[1]) circs();
	if (sc_flags[2]) spiral();
    // if (sc_flags[3]) figCirc();
	if (sc_flags[4]) rays();
	if (sc_flags[5]) spectrum();
	if (sc_flags[6]) confetti();
	// if (sc_flags[7]) lines();
	// if (sc_flags[8]) tunel();
	if (sc_flags[9]) tentacles();
	if (sc_flags[10]) volConcentrico();

	if (help) helpF();

	// if (camaraON) {
	// 	camara();
	// }

	camara();

    genMaskF();
}

//--------------------------------------------------------------
void ofApp::draw() {

    /// GRAFICA
    for (size_t i = 0; i < NFBO; i++) {
        if (sc_flags[i]) fbo[i].draw(coordFbo[0].x + posMap.x, coordFbo[0].y + posMap.y);
    }

    /// CAMARA
    if (camaraON) {
        fboCam.draw(coordFbo[0].x + posMap.x, coordFbo[0].y + posMap.y);
    }

	/// MASCARILLA
	fboMaster.draw(coordFbo[0].x + posMap.x, coordFbo[0].y + posMap.y);

	/// HELP
	if (help) fboHelp.draw(coordFbo[0].x + posMap.x + ancho*.25, coordFbo[0].y + posMap.y + alto*.1);

    fboGenMask.draw(0,0);
}

//---- MASCARILLA ----------------------------------------------------------
void ofApp::mascarilla() {
	fboMaster.begin();

	ofBackground(0, 0);
	ofPushStyle();
	ofSetColor(0);
	ofFill();
	ofBeginShape();
	ofVertex(centroX, 0);

	for (int i = 0; i < numSegMasc; i++) {
		ofVertex(centroX + cos(-HALF_PI + ofDegToRad(360 / numSegMasc)*i)*radioMasc, centroY + sin(-HALF_PI + ofDegToRad(360 / numSegMasc)*i)*radioMasc);
	}

	ofVertex(centroX + cos(-HALF_PI)*radioMasc, centroY + sin(-HALF_PI)*radioMasc);

	ofVertex(centroX, 0);
	ofVertex(ancho, 0);
	ofVertex(ancho, alto);
	ofVertex(0, alto);
	ofVertex(0, 0);

	ofEndShape();
    if (mapeaTeclas || mapeaMidi) {
        ofSetColor(255, 0, 0);
        ofFill();
        ofRect(centroX - 1, centroY - 50, 2, 100);
        ofRect(centroX - 50, centroY - 1, 100, 2);
    }
	ofPopStyle();

	fboMaster.end();
}
//--------------------------------------------------------------
void ofApp::helpF() {
	fboHelp.begin();

	ofClear(0, 80);
	ofSetColor(255);
	ofDrawBitmapString("A Y U D A", fboHelp.getWidth()*.2, 100);
	fboHelp.end();
}

//--------------------------------------------------------------
void ofApp::genMaskF() {
	fboGenMask.begin();
	ofClear(0, controller[4]);
	fboGenMask.end();
}

//--------------------------------------------------------------
void ofApp::exit() {
	// clean up
	midiIn.closePort();
	midiIn.removeListener(this);
}

//--------------------------------------------------------------
void ofApp::audioReceived(float* input, int bufferSize, int nChannels) {
	beat.audioReceived(input, bufferSize, nChannels);
}

//--------------------------------------------------------------
void ofApp::newMidiMessage(ofxMidiMessage& msg) {
	// make a copy of the latest message
	midiMessage = msg;

	switch (midiMessage.control) {
	//////// SCENES PADS ////////
	case 24:
		sc_flags[0] = !sc_flags[0];
		break;
	case 25:
		sc_flags[1] = !sc_flags[1];
		break;
	case 26:
		sc_flags[2] = !sc_flags[2];
		break;
	case 27:
		// sc_flags[3] = !sc_flags[3];
		sc_flags[4] = !sc_flags[4];
		break;
	case 28:
		sc_flags[5] = !sc_flags[5];
		break;
	case 29:
		// sc_flags[7] = !sc_flags[7];
		sc_flags[6] = !sc_flags[6];
		break;
    case 31:
        sc_flags[10] = !sc_flags[10];
        if (!sc_flags[10]) {
            totalVols = 2;
            volY.resize(totalVols);
            for (int i = 0; i < totalVols; i++) {
                volY.push_back(0);
            }
        }
        break;
	case 30:
		sc_flags[9] = !sc_flags[9];
		break;
	case 1:
		mapeaMidi = !mapeaMidi;
		break;
	case 2:
		help = !help;
		break;
	case 11: //////// CAMERA FADER ////////
		controller[0] = ofMap(msg.value, 0, 127, 0, 255);
        // printf("%d\n", 255-controller[0]);
		break;
    case 22: //////// CAMERA EFFECT ////////
		escenaCamAct = int(ofMap(msg.value, 0, 127, 0, numEscenasCam)); // escenas de Camara
		break;
	case 21: //////// CAMERA RESOLUTION ////////
		sep = int(ofMap(msg.value,0,127,20,50)); // separacion de pixeles escena camara
		break;
	case 15: //////// X SCREEN ADJUST ////////
		controller[1] = msg.value;
		break;
	case 16: //////// Y SCREEN ADJUST ////////
		controller[2] = msg.value;
		break;
	case 17: //////// ADJUST SCREEN SHAPE ////////
		controller[3] = msg.value;
		break;
	case 12: //////// MASTER ALPHA ////////
		controller[4] = ofMap(msg.value, 0, 127, 0, 255);
		break;

    //////// PARAMETER CONTROLLERS ////////

	case 4:
		controller[5] = ofMap(msg.value, 0, 127, 0, 220);
		break;
	case 5:
		controller[6] = ofMap(msg.value, 0, 127, 0, 220);
		break;
	case 6:
		controller[7] = ofMap(msg.value, 0, 127, 0, 220);
		break;
	case 7:
		controller[8] = ofMap(msg.value, 0, 127, 0, 220);
		break;
	case 8:
		controller[9] = ofMap(msg.value, 0, 127, 0, 220);
		break;
	case 9:
		controller[9] = ofMap(msg.value, 0, 127, 0, 220);
		break;
	case 10:
		controller[10] = ofMap(msg.value, 0, 127, 0, 220);
		break;

	case 18:
		controller[11] = msg.value;
        // globalColor.setHue(ofMap(msg.value,0,127,0,254));
		break;
	case 19:
		controller[12] = msg.value;
		break;
	case 20:
		controller[13] = msg.value;
		break;
	}

	numSegMasc = ofMap(controller[3], 0, 127, 3, 10, true);
	if (numSegMasc >= 9) numSegMasc = 60;
}

//--------------------------------------------------------------
void ofApp::moon() {

    float scale;

    scale = ofMap(beat.getBand(6), 0.2, 1.5, 0.6, 1.2, true);

	fbo[0].begin();

	ofSetColor(0, 25);
	ofDrawRectangle(0, 0, ofGetWidth(), ofGetHeight());

	ofTranslate(ofGetWidth() / 2, ofGetHeight() / 2);

    if(controller[5] >= 10){
        sceneColor[0].setHsb(globalColor.getHue() + controller[5], 255, 255, ofMap(beat.getBand(1), 0.2, 1.5, 50, 200, true));
        ofSetColor(sceneColor[0]);
    }else{
        ofSetColor(255, ofMap(beat.getBand(1), 0.2, 1.5, 50, 200, true));
    }

    ofPushMatrix();

    ofScale(scale, scale, scale);

    if (beat.getBand(4) >= 0.4) {
		for (size_t i = 0; i < 100; i++) {

			float ang = ofRandom(TWO_PI);
			float ang1 = ofRandom(TWO_PI);

			ofDrawLine(R*cos(ang), R*sin(ang), R*cos(ang1), R*sin(ang1));
		}
	}

    ofPopMatrix();

    ofFill();
    if (controller[0] >= 20) {
        ofSetColor(0, 100 - controller[0]/2);
        // printf("%d,%d\n",controller[0], 255-controller[0]);
        ofDrawRectangle(-ofGetWidth() / 2, -ofGetHeight() / 2, ofGetWidth(), ofGetHeight());
    }


	fbo[0].end();
}

//--------------------------------------------------------------
void ofApp::circs() {

	int rad = ofGetHeight() / 2 / 12;
	float n_circs = 22.f;
	float ang = TWO_PI / n_circs;
	float scale;

	fbo[1].begin();

	ofSetColor(0, 25);
	ofDrawRectangle(0, 0, ofGetWidth(), ofGetHeight());

	ofTranslate(ofGetWidth() / 2, ofGetHeight() / 2);

	ofNoFill();

    if(controller[6] >= 10){
        sceneColor[1].setHsb(globalColor.getHue() + controller[6], 255, 255, ofMap(beat.getBand(1), 0.2, 1.5, 90, 250, true));
        ofSetColor(sceneColor[1]);
    }else{
        ofSetColor(255, ofMap(beat.getBand(1), 0.2, 1.5, 90, 250, true));
    }

	for (size_t i = 1; i < 12; i++) {
		ofPushMatrix();
		scale = ofMap(beat.kick(), 0.0, 1.0, 0.5, 1., true);

		if (beat.getBand(5) >= 0.5) ofRotate(ofMap(beat.getBand(5), 0.5, 1.0, i + ofRandom(-10, 10), i + ofRandom(-2, 2), true));
		if (beat.getBand(1) >= 0.8) ofScale(scale, scale, scale);

		for (size_t j = 0; j < n_circs; j++) {
			ofDrawCircle(i * rad * cos(j * ang), i * rad * sin(j * ang), i*i* sin(ofGetElapsedTimeMillis() / 500));
		}
		ofPopMatrix();
	}

    ofFill();
    if (controller[0] >= 20) {
        ofSetColor(0, 100 - controller[0]/2);
        // printf("%d,%d\n",controller[0], 255-controller[0]);
        ofDrawRectangle(-ofGetWidth() / 2, -ofGetHeight() / 2, ofGetWidth(), ofGetHeight());
    }

	fbo[1].end();
}

//--------------------------------------------------------------
void ofApp::spiral() {

	int rad = ofGetHeight() / 2 / 25;
	float n_circs = 20.f;
	float ang = TWO_PI / n_circs;
	float alpha;
	float scale;

	fbo[2].begin();

	ofSetColor(0, 25);
	ofDrawRectangle(0, 0, ofGetWidth(), ofGetHeight());

	ofSetColor(255, ofMap(beat.kick(), 0.0, 1.0, 0, 80));

	ofDrawRectangle(0, 0, ofGetWidth(), ofGetHeight());

	ofTranslate(ofGetWidth() / 2, ofGetHeight() / 2);

	// ofNoFill();
	// alpha = ofMap(beat.hihat(), 0.0, 1.0, 50, 220, true);
	// alpha = ofMap(beat.kick(), 0.0, 1.0, 50, 250, true);
	alpha = ofMap(beat.getBand(2), 0.3, 1.2, 50, 250, true);
	// scale = ofMap(beat.kick(), 0.0, 1.0, 0.5, 1.2, true);

	// alpha = ofMap(beat.getBand(5), 0.2, 1.5, 1, 200, true);
	// scale = ofMap(beat.getBand(6), 0.1, 1.3, 0.1, 1.1, true);

	scale = ofMap(beat.getBand(5), 0.3, 1.0, 0.65, 1.0f, true);

	for (size_t i = 1; i < 25; i++) {

        // ofSetColor(255 * (i % 2), i + alpha);

        if(controller[7] >= 10){
            sceneColor[2].setHsb(globalColor.getHue() + controller[7] * (i % 2), 255, 255, i + alpha);
            ofSetColor(sceneColor[2]);
        }else{
            ofSetColor(255 * (i % 2), i + alpha);
        }


		ofPushMatrix();

		// if(beat.getBand(4) >= 0.3) ofRotate( 2 * i * sin(ofGetElapsedTimef()));
		if (beat.getBand(4) >= 0.3) ofRotate(4 * i * ofGetElapsedTimef());
		// ofRotate( 10*i + 10 * ofGetElapsedTimef());

		scale = fmod(i + ofGetElapsedTimef() / 2.2, 1.1);

		ofScale(scale, scale, scale);

		for (int j = 0; j < n_circs; j++) {

			ofDrawCircle(i * rad * cos(j * ang), i * rad * sin(j * ang), 2 * i);
			ofDrawCircle(i * rad * sin(j * ang), i * rad * cos(j * ang), 2 * i);

			ofDrawCircle(i * rad * cos(-j * ang), i * rad * sin(-j * ang), 2 * i);
			ofDrawCircle(i * rad * sin(-j * ang), i * rad * cos(-j * ang), 2 * i);
		}

		ofPopMatrix();
	}

    ofFill();
    if (controller[0] >= 20) {
        ofSetColor(0, 100 - controller[0]/2);
        // printf("%d,%d\n",controller[0], 255-controller[0]);
        ofDrawRectangle(-ofGetWidth() / 2, -ofGetHeight() / 2, ofGetWidth(), ofGetHeight());
    }

	fbo[2].end();
}

//--------------------------------------------------------------
void ofApp::figCirc() {

	float ang = ofGetElapsedTimef();

	float alpha;
	float scale;

	fbo[3].begin();

	ofSetColor(0, 1);
	ofDrawRectangle(0, 0, ofGetWidth(), ofGetHeight());

	ofTranslate(ofGetWidth() / 2, ofGetHeight() / 2);

	ofSetColor(255, 200);

	ofDrawLine(r*cos(2 * ang), r*sin(2 * ang), R*cos(ang), R*sin(ang));

	// for (size_t i = 1; i < 15; i++) {
	// ofDrawLine(i*r*cos(i*ang), i*r*sin(i*ang), (i+1)*r*cos((i+1)*ang), (i+1)*r*sin((i+1)*ang));
	// ofDrawLine(i*r*cos(-i*ang), i*r*sin(-i*ang), (i+1)*r*cos(-(i+1)*ang), (i+1)*r*sin((-i+1)*ang));
	// }

	fbo[3].end();
}

//--------------------------------------------------------------

void ofApp::rays() {

	float n_circs = 100.f;
	float ang = TWO_PI / n_circs;
	float offSet = 100;
	float r = ofGetHeight() / 8;
	float R = ofGetHeight() / 2.5;

	float alpha;
	float scale;

	fbo[4].begin();

	ofSetColor(0, 15);
	ofDrawRectangle(0, 0, ofGetWidth(), ofGetHeight());

	ofTranslate(ofGetWidth() / 2, ofGetHeight() / 2);

	ofRotate(10 * ofGetElapsedTimef());

	// ofSetColor(255, 200);

    if(controller[8] >= 10){
        sceneColor[4].setHsb(globalColor.getHue() + controller[8], 255, 255, ofMap(beat.getBand(1), 0.2, 1.5, 200, 255, true));
        ofSetColor(sceneColor[4]);
    }else{
        ofSetColor(255, ofMap(beat.getBand(1), 0.2, 1.5, 200, 255, true));
    }

	for (int i = 0; i < n_circs; i++) {
		ofDrawLine(r*cos(i*ang), r*sin(i*ang), (R + ofMap(beat.getBand(i % 16), 0.0, 1.2, -offSet, offSet))*cos(i*ang), (R + ofMap(beat.getBand(i % 16), 0.0, 1.2, -offSet, offSet))*sin(i*ang));
	}

    ofFill();
    if (controller[0] >= 20) {
        ofSetColor(0, 100 - controller[0]/2);
        // printf("%d,%d\n",controller[0], 255-controller[0]);
        ofDrawRectangle(-ofGetWidth() / 2, -ofGetHeight() / 2, ofGetWidth(), ofGetHeight());
    }

	fbo[4].end();
}

//--------------------------------------------------------------
void ofApp::spectrum() {

	float sx = ofGetHeight() / 2 / 16;
	float bandVal;
	float rectW;
	float rad;

	fbo[5].begin();

	ofSetColor(0, 25);
	ofDrawRectangle(0, 0, ofGetWidth(), ofGetHeight());

	ofPushMatrix();

	ofTranslate(ofGetWidth() / 2, ofGetHeight() / 2);

	ofNoFill();
	// ofSetColor(255, 200);

    if(controller[9] >= 10){
        sceneColor[5].setHsb(globalColor.getHue() + controller[9], 255, 255, ofMap(beat.getBand(1), 0.2, 1.5, 200, 255, true));
        ofSetColor(sceneColor[5]);
    }else{
        ofSetColor(255, ofMap(beat.getBand(1), 0.2, 1.5, 200, 255, true));
    }

	rad = fmod(ofGetElapsedTimeMillis(), float(ofGetHeight() / 2));

	ofSetLineWidth(2);
	if (beat.kick() > 0.0) ofDrawCircle(0, 0, rad, rad);

	rectW = ofMap(beat.kick(), 0.0, 1.0, 5, 15);

	ofSetRectMode(OF_RECTMODE_CENTER);

	ofFill();
	ofSetLineWidth(1);

	for (int i = 0; i < 15; i++) {

		bandVal = ofMap(beat.getBand(i), 0.0, 1.5, 0.0, ofGetHeight() / 2);

		ofDrawRectangle(i*sx, 0, rectW, bandVal);
		ofDrawRectangle(-i*sx, 0, rectW, bandVal);

		// ofDrawRectangle(i*sx, 0, rectW, -bandVal);
		// ofDrawRectangle(-i*sx, 0, rectW, -bandVal);
	}

	ofSetRectMode(OF_RECTMODE_CORNER);

    ofFill();
    if (controller[0] >= 20) {
        ofSetColor(0, 100 - controller[0]/2);
        // printf("%d,%d\n",controller[0], 255-controller[0]);
        ofDrawRectangle(-ofGetWidth() / 2, -ofGetHeight() / 2, ofGetWidth(), ofGetHeight());
    }

	ofPopMatrix();


	fbo[5].end();
}

//--------------------------------------------------------------

void ofApp::confetti() {

	float r;
	float ang;
	float rad;

	fbo[6].begin();

	ofSetColor(0, 25);
	ofDrawRectangle(0, 0, ofGetWidth(), ofGetHeight());

	ofPushMatrix();

	ofTranslate(ofGetWidth() / 2, ofGetHeight() / 2);

	rad = fmod(25 * ofGetElapsedTimef(), float(ofGetHeight() / 2));

	ofNoFill();
    ofSetColor(255, ofMap(beat.getBand(6), 0.2, 1.2, 0, 220));

	ofDrawCircle(0, 0, rad + ofMap(beat.getBand(2), 0.0, 1.0, -20, 20));

	ofFill();
    if(controller[9] >= 10){
        sceneColor[6].setHsb( globalColor.getHue() + controller[9], 255, 255, ofMap(beat.getBand(5), 0.2, 1.2, 50, 250));
        ofSetColor(sceneColor[6]);
    }else{
        ofSetColor(255, ofMap(beat.getBand(5), 0.2, 1.2, 50, 250));
    }

	if (beat.kick() > 0.) {
		for (int i = 0; i < 50; i++) {

			r = ofRandom(5, ofGetHeight() / 2);
			ang = ofRandom(TWO_PI);

			ofDrawCircle(r*cos(ang), r*sin(ang), ofRandom(2, 25));
		}
	}

    ofFill();
    if (controller[0] >= 20) {
        ofSetColor(0, 100 - controller[0]/2);
        // printf("%d,%d\n",controller[0], 255-controller[0]);
        ofDrawRectangle(-ofGetWidth() / 2, -ofGetHeight() / 2, ofGetWidth(), ofGetHeight());
    }

	ofPopMatrix();

	fbo[6].end();
}

//--------------------------------------------------------------

void ofApp::lines() {

	int n_r = 16;
	float sy = (ofGetHeight() / 2) / float(n_r);
	float bandVal;
	float hBandVal;
	float rel = fmod(ofGetElapsedTimeMillis(), 0.1);
	ofColor c = ofColor(0);

	fbo[7].begin();

	ofSetColor(0, 25);

	ofDrawRectangle(0, 0, ofGetWidth(), ofGetHeight());

	ofPushMatrix();

	ofTranslate(ofGetWidth() / 2, ofGetHeight() / 2);

	ofFill();
	ofSetRectMode(OF_RECTMODE_CENTER);

	// ofSetColor( 255, 0, 0, ofMap(beat.kick(), 0.0, 1.0, 0, 200));
	c.setHsb(ofMap(beat.getBand(1), 0.0, 1.0, 120, 180), 255, 255);
	ofSetColor(c, ofMap(beat.kick(), 0.0, 1.0, 10, 200));

	ofDrawCircle(0, 0, ofGetHeight() / 2, ofGetHeight() / 2);

	for (int i = 0; i < n_r; i++) {

		bandVal = ofMap(beat.getBand(i) - rel, 0.0, 1.5, 0.0, ofGetHeight() / 2);
		hBandVal = ofMap(beat.getBand(i) - rel, 0.0, 1.5, 1, 50);

		ofSetColor(5 * 25 * (i % 4), ofMap(beat.getBand(i), 0.2, 1.2, 50, 200));

		ofDrawRectRounded(0, -i*sy, bandVal, hBandVal, 10);
		ofDrawRectRounded(0, i*sy, bandVal, hBandVal, 10);
	}

	ofSetRectMode(OF_RECTMODE_CORNER);

    ofFill();
    if (controller[0] >= 20) {
        ofSetColor(0, 100 - controller[0]/2);
        // printf("%d,%d\n",controller[0], 255-controller[0]);
        ofDrawRectangle(-ofGetWidth() / 2, -ofGetHeight() / 2, ofGetWidth(), ofGetHeight());
    }

	ofPopMatrix();

	fbo[7].end();
}

//--------------------------------------------------------------

void ofApp::tunel() {

	int n_r = 24;
	float o_x = 0.0;
	float o_y = 0.0;
	float v_i;

	float s_r = ofGetHeight() / 2 / float(n_r);
	ofColor c = ofColor(0);

	fbo[8].begin();

	ofSetColor(0, 25);

	ofDrawRectangle(0, 0, ofGetWidth(), ofGetHeight());

	ofPushMatrix();

	ofTranslate(ofGetWidth() / 2, ofGetHeight() / 2);

	ofFill();
	// ofSetRectMode(OF_RECTMODE_CENTER);

	// ofSetColor( 255, 0, 0, ofMap(beat.kick(), 0.0, 1.0, 0, 200));
	// c.setHsb(ofMap(beat.getBand(1), 0.0, 1.0, 120, 180), 255, 255);
	// ofSetColor( c, ofMap(beat.kick(), 0.0, 1.0, 10, 200));

	// ofDrawCircle(0,0,ofGetHeight()/2, ofGetHeight()/2);

	// o_r = ofMap(beat.kick(), 0.0, 1.0, -3, 3);
	// if(beat.getBand(1) > 0.6) o_r = ofRandom(-1.0,1.0);

	for (int i = n_r; i > 0; i--) {

		if (beat.getBand(i % 16) > 0.4) {
			// o_x = ofNoise(i*ofGetElapsedTimef());
			// o_y = ofNoise(i*ofGetElapsedTimef());
			// o_x = ofMap(beat.getBand(i % 16), 0.4, 1.0, -10, 10) * (-1.0 + ofNoise(i*ofGetElapsedTimef()));
			// o_y = ofMap(beat.getBand(i % 16), 0.4, 1.0, -10, 10) * (-1.0 + ofNoise(i*ofGetElapsedTimef()));
			o_x = ofMap(beat.getBand(i % 16), 0.4, 1.0, -10, 10);
			o_y = ofMap(beat.getBand(i % 16), 0.4, 1.0, -10, 10);
		}

		v_i = ofMap(i, 0, n_r, 5, 1);

		// ofSetColor( 5 * 25*(i % 4), ofMap(beat.getBand(i), 0.2, 1.2, 50, 200));
		ofSetColor(5 * 12 * (i % 8), 255);

		// ofDrawCircle(i*cos(ofGetElapsedTimef() + o_r), i*sin(ofGetElapsedTimef() + o_r), 2*i + s_r, 2*i + s_r);
		// ofDrawCircle(-i*cos(ofGetElapsedTimef() + o_r), -i*sin(ofGetElapsedTimef() + o_r), 2*i + s_r, 2*i + s_r);

		// ofDrawCircle(i*cos(i + ofGetElapsedTimef()) + i*o_r, i*sin(i + ofGetElapsedTimef()) + i*o_r, s_r*i, s_r*i);
		// ofDrawCircle((n_r - 1 + s_r * i)*cos(i + ofGetElapsedTimef()) + o_r, (n_r - 1 + s_r * i)*sin(i + ofGetElapsedTimef()) + o_r, s_r*i, s_r*i);

		// ofDrawCircle(i*cos(v_i*ofGetElapsedTimef()) + o_x, i*sin(v_i*ofGetElapsedTimef())+ o_y, s_r*i, s_r*i);
		ofDrawCircle(o_x, o_y, s_r*i, s_r*i);

	}

	ofSetRectMode(OF_RECTMODE_CORNER);
	ofPopMatrix();

	fbo[8].end();
}

//--------------------------------------------------------------

void ofApp::tentacles() {

	int n_r = 24;
	float o_x = 0.0;
	float o_y = 0.0;
	float v_i;
	int arms = 8;
	float ang = TWO_PI / float(arms);
	float scale = 2.5;

	float s_r = ofGetHeight() / 2 / float(n_r);
	ofColor c = ofColor(0);

	fbo[9].begin();

	ofSetColor(0, 15);

	ofDrawRectangle(0, 0, ofGetWidth(), ofGetHeight());

	ofTranslate(ofGetWidth() / 2, ofGetHeight() / 2);

	ofFill();

	c.setHsb(ofMap(beat.getBand(12), 0.0, 1.0, 120, 180), 255, 255);
	ofSetColor(c, ofMap(beat.hihat(), 0.0, 1.0, 10, 200));

	ofDrawCircle(0, 0, ofGetHeight() / 2, ofGetHeight() / 2);


	for (int j = 0; j < arms; j++) {
		for (int i = n_r; i > 0; i--) {
			ofPushMatrix();

			// scale = ofMap(beat.getBand(i % 16), 0.2, 1.2, -10, 10);
			scale = ofMap(beat.getBand(i % 16), 0.2, 1.2, -0.25, 0.25);

			ofScale(0.8 + scale, 0.8 + scale, 0.8 + scale);

			v_i = ofMap(i, 0, n_r, 1.5, 0.05);

            if(controller[10] >= 10){
                sceneColor[9].setHsb(globalColor.getHue() + controller[10] * (i % 8), 255, 255, ofMap(beat.getBand(5), 0.2, 1.2, 50, 250));
                ofSetColor(sceneColor[9]);
            }else{
                ofSetColor(60 * (i % 8), ofMap(beat.getBand(5), 0.2, 1.2, 50, 250));
            }

			ofDrawCircle((n_r - i)*s_r*cos(j*ang) * cos(v_i * ofGetElapsedTimef()), (n_r - i)*s_r*sin(j*ang) * sin(v_i * ofGetElapsedTimef()), 2.5*i + scale, 2.5*i + scale);
			ofDrawCircle(-(n_r - i)*s_r*cos(j*ang) * cos(v_i * ofGetElapsedTimef()), -(n_r - i)*s_r*sin(j*ang) * sin(v_i * ofGetElapsedTimef()), 2.5*i + scale, 2.5*i + scale);

			// ofDrawCircle((n_r - i)*s_r*cos(j*ang) * cos(v_i * ofGetElapsedTimef()), (n_r - i)*s_r*sin(j*ang) * sin(v_i * ofGetElapsedTimef()), scale*i, scale*i);
			// ofDrawCircle(-(n_r - i)*s_r*cos(j*ang) * cos(v_i * ofGetElapsedTimef()), -(n_r - i)*s_r*sin(j*ang) * sin(v_i * ofGetElapsedTimef()), scale*i, scale*i);
			ofPopMatrix();
		}
	}

    ofFill();
    if (controller[0] >= 20) {
        ofSetColor(0, 100 - controller[0]/2);
        // printf("%d,%d\n",controller[0], 255-controller[0]);
        ofDrawRectangle(-ofGetWidth() / 2, -ofGetHeight() / 2, ofGetWidth(), ofGetHeight());
    }

	fbo[9].end();
}
//--------------------------------------------------------------
void ofApp::volConcentrico() {
	fbo[10].begin();
	// ofSetColor(0, opa);
	ofSetColor(0, 25);
	ofFill();
	ofRect(0, 0, ofGetWidth(), ofGetHeight());
	ofSetLineWidth(abs(beat.getBand(6))*50);

	if (totalVols < 30) {
		if (ofGetFrameNum() % 15 == 0)
			totalVols++;
	}
	/*else {
		velRot = int(ofMap(controller[9], 0, 127, 3, 8));
	}*/
	if (ofGetFrameNum() % velRot == 0) {
		volY.resize(totalVols);
		volY.push_back(abs(beat.getBand(8)));

		if (volY.size() >= totalVols) {
			volY.erase(volY.begin());
		}
	}

	// ofSetColor(255);

    if(controller[10] >= 10){
        sceneColor[9].setHsb(globalColor.getHue() + controller[10], 255, 255, ofMap(beat.getBand(5), 0.2, 1.2, 50, 250));
        ofSetColor(sceneColor[9]);
    }else{
        ofSetColor(255, ofMap(beat.getBand(5), 0.2, 1.2, 50, 250));
    }

	ofFill();
	if (ofGetFrameNum() % 120 == 0) {
		activaContenido++;
		activaContenido = activaContenido % numCont;
		opa = int(ofRandom(255));
	}

	for (int i = 0; i < totalVols; i++) {
		float volLocal = volY[i];
		float pExtX = cos(ofDegToRad((360 / totalVols)*i))*ofGetHeight();
		float pExtY = sin(ofDegToRad((360 / totalVols)*i))*ofGetHeight();
		float pIntX = cos(ofDegToRad((360 / totalVols)*i))*(100 + volLocal * 120);
		float pIntY = sin(ofDegToRad((360 / totalVols)*i))*(100 + volLocal * 120);

		ofLine(centroX + pExtX, centroY + pExtY, centroX + pIntX, centroY + pIntY);

		switch (activaContenido) {
		case 0:
			break;
		case 1:
			ofCircle(centroX + pIntX*1.2, centroY + pIntY*1.2, volLocal * 50, volLocal * 50);
			break;
		case 2:
			ofRect(centroX + pIntX*1.2, centroY + pIntY*1.2, volLocal * 50, volLocal * 50);
			break;
		}
	}

    ofFill();
    if (controller[0] >= 20) {
        ofSetColor(0, 100 - controller[0]/2);
        // printf("%d,%d\n",controller[0], 255-controller[0]);
        ofDrawRectangle(0, 0, ofGetWidth(), ofGetHeight());
    }

	fbo[10].end();
}
//--------------------------------------------------------------

void ofApp::colorRainhaha() {
	fbo[11].begin();
	fbo[11].end();
}

//--------------------------------------------------------------
void ofApp::camara() {
	fboCam.begin();
	ofPixels & pixels = vidGrabber.getPixels();
	pixels.setImageType(OF_IMAGE_COLOR_ALPHA);
	ofClear(0, 0);

	if (escenaCamAct == 0) {
		videoTexture.draw(ofGetWidth() / 2 - camWidth / 2 * escalaVideo, ofGetHeight() / 2 - camHeight / 2 * escalaVideo, camWidth*escalaVideo, camHeight*escalaVideo);
	}
	else {
		ofPushStyle();
		ofColor col;
		float brillo;
		float scBrillo;
		for (int j = 0; j < camHeight; j += sep) {
			for (int i = 0; i < camWidth; i += sep) {
				int indice = (i + j*camWidth) * 4;
				switch (escenaCamAct) {
				case 1:
					ofSetColor(pixels[indice], pixels[indice + 1], pixels[indice + 2], controller[0]);
					ofFill();
					ofRect((ofGetWidth() / 2 - camWidth / 2 * escalaVideo) + i*escalaVideo, (ofGetHeight() / 2 - camHeight / 2 * escalaVideo) + j*escalaVideo, (sep - 2)*escalaVideo, (sep - 2)*escalaVideo);
					break;
				case 2:
					ofSetColor(pixels[indice], pixels[indice + 1], pixels[indice + 2], controller[0]);
					ofFill();
					ofEllipse((ofGetWidth() / 2 - camWidth / 2 * escalaVideo) + i*escalaVideo, (ofGetHeight() / 2 - camHeight / 2 * escalaVideo) + j*escalaVideo, (sep - 2)*escalaVideo, (sep - 2)*escalaVideo);
					break;
				case 3:
					col = ofColor(pixels[indice], pixels[indice + 1], pixels[indice + 2], controller[0]);
					brillo = col.getBrightness();
					scBrillo = ofMap(brillo, 0, 255, 0, 2);
					if (brillo > 120) {
						ofSetColor(pixels[indice], pixels[indice + 1], pixels[indice + 2], controller[0]);
						ofFill();
						ofEllipse((ofGetWidth() / 2 - camWidth / 2 * escalaVideo) + i*escalaVideo, (ofGetHeight() / 2 - camHeight / 2 * escalaVideo) + j*escalaVideo, (sep - 2)*escalaVideo*scBrillo, (sep - 2)*escalaVideo*scBrillo);
					}
					break;
				case 4:
					col = ofColor(pixels[indice], pixels[indice + 1], pixels[indice + 2], controller[0]);
					brillo = col.getBrightness();
					scBrillo = ofMap(brillo, 0, 255, 0, 2);
					if (brillo > 120) {
						ofSetColor(brillo);
						ofFill();
						ofEllipse(
							(ofGetWidth() / 2 - camWidth / 2 * escalaVideo) + i*escalaVideo,
							(ofGetHeight() / 2 - camHeight / 2 * escalaVideo) + j*escalaVideo,
							(sep - 2)*escalaVideo*scBrillo,
							(sep - 2)*escalaVideo*scBrillo
						);
					}
					break;
				case 5:
					col = ofColor(pixels[indice], pixels[indice + 1], pixels[indice + 2], controller[0]);
					brillo = col.getBrightness();
					scBrillo = ofMap(brillo, 0, 255, 0, 2);
					if (brillo > 80) {
						ofSetColor(brillo);
						ofFill();
						float ampV = abs(beat.getBand((i + j*camWidth) % 16)) * 20;
						ofEllipse(
							(ofGetWidth() / 2 - camWidth / 2 * escalaVideo) + i*escalaVideo,
							(ofGetHeight() / 2 - camHeight / 2 * escalaVideo) + j*escalaVideo,
							(sep - 2)*escalaVideo*scBrillo + ampV,
							(sep - 2)*escalaVideo*scBrillo + ampV
						);

					}
					break;
				}
			}
		}
		ofPopStyle();
	}
	fboCam.end();
}

//--------------------------------------------------------------
void ofApp::keyPressed(int key) {
	switch (key) {
	case '0':
		sc_flags[0] = !sc_flags[0];
		break;
	case '1':
		sc_flags[1] = !sc_flags[1];
		break;
	case '2':
		sc_flags[2] = !sc_flags[2];
		break;
	case '3':
		sc_flags[3] = !sc_flags[3];
		break;
	case '4':
		sc_flags[4] = !sc_flags[4];
		break;
	case '5':
		sc_flags[5] = !sc_flags[5];
		break;
	case '6':
		sc_flags[6] = !sc_flags[6];
		break;
	case '7':
		sc_flags[7] = !sc_flags[7];
		break;
	case '8':
		sc_flags[8] = !sc_flags[8];
		break;
	case '9':
		sc_flags[9] = !sc_flags[9];
		break;
	case 'l':
		midiIn.listPorts();
		break;
	}
	if (mapeaTeclas) {
		if (key == OF_KEY_RIGHT) {
			posMap.x++;
		}
		if (key == OF_KEY_LEFT) {
			posMap.x--;
		}
		if (key == OF_KEY_UP) {
			posMap.y--;
		}
		if (key == OF_KEY_DOWN) {
			posMap.y++;
		}
	}
	if (key == OF_KEY_ALT) {
		alt = true;
	}
	if (alt) {
		if (key == 'f' || key == 'F') {
			fs = !fs;
			if (fs)
				ofSetFullscreen(true);
			else
				ofSetFullscreen(false);
		}
		if (key == 'm' || key == 'M') {
			mapeaTeclas = !mapeaTeclas;
		}
		if (key == 'i' || key == 'I') {
			mapeaMidi = !mapeaMidi;
		}
		if (key == 'h' || key == 'H')
			help = !help;
	}
}

//--------------------------------------------------------------
void ofApp::keyReleased(int key) {
	if (!mapeaTeclas) {
		if (key == OF_KEY_RIGHT) {
			ofLogNotice(ofToString(numSegMasc));
			if (numSegMasc < 60)
				numSegMasc++;
			if (numSegMasc == 11 || numSegMasc == 13 || numSegMasc == 14 || numSegMasc == 16 || numSegMasc == 19)
				numSegMasc++;
			if (numSegMasc == 20)
				numSegMasc = 60;
		}
		if (key == OF_KEY_LEFT) {
			if (numSegMasc > 3)
				numSegMasc--;
			if (numSegMasc == 11 || numSegMasc == 13 || numSegMasc == 14 || numSegMasc == 16 || numSegMasc == 19)
				numSegMasc--;
			if (numSegMasc == 59)
				numSegMasc = 20;
		}
	}
	if (key == OF_KEY_ALT) {
		alt = false;
	}
}


//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y) {

}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button) {
	if (mapeaTeclas) {
		posMap.x = mouseX - centroX;
		posMap.y = mouseY - centroY;
	}
}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button) {

}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button) {

}

//--------------------------------------------------------------
void ofApp::mouseEntered(int x, int y) {

}

//--------------------------------------------------------------
void ofApp::mouseExited(int x, int y) {

}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h) {

}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg) {

}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo) {

}
