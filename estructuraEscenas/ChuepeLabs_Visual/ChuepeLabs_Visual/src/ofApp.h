#pragma once

#include "ofMain.h"
#include "ofxMidi.h"
#include "ofxBeat.h"
#include <cmath>

#define NFBO 12
#define NCONT 14

class ofApp : public ofBaseApp, public ofxMidiListener {

	//////// BEAT DETECTION ////////
	ofxBeat beat;

public:
	void setup();
	void update();
	void draw();
	void exit();

	void keyPressed(int key);
	void keyReleased(int key);
	void mouseMoved(int x, int y);
	void mouseDragged(int x, int y, int button);
	void mousePressed(int x, int y, int button);
	void mouseReleased(int x, int y, int button);
	void mouseEntered(int x, int y);
	void mouseExited(int x, int y);
	void windowResized(int w, int h);
	void dragEvent(ofDragInfo dragInfo);
	void gotMessage(ofMessage msg);

	//////// AUDIO INPUT ////////
	void audioReceived(float*, int, int);
	// void audioIn(float * input, int bufferSize, int nChannels);

	ofSoundStream soundStream;

	//////// MIDI LISTENER ////////
	void newMidiMessage(ofxMidiMessage& eventArgs);
	stringstream text;

	ofxMidiIn midiIn;
	ofxMidiMessage midiMessage;

    //////// CAMERA ////////
    ofVideoGrabber vidGrabber;
    ofPixels videoWithAlpha;
    ofTexture videoTexture;
    int camWidth;
    int camHeight;
	bool camaraON = true;
	float escalaVideo = 2.25f;

	/// FULLSCREEN
	bool fs = true;
	bool alt;

	//////// SCENES DISPLAY FLAGS ////////
	bool sc_flags[NFBO];

	//////// SCENES DISPLAY FLAGS ////////
	int controller[NCONT];

	//////// FBO INSTANCES ////////
	ofFbo fbo[NFBO];
	ofFbo fboCam;

    ofColor sceneColor[NFBO];

	//////// FBO INSTANCES ////////
	int curr_FBO;
	int next_FBO;

    float transInter;

	//////// FBO SCENES FUNCTIONS ////////
	void moon();
	void circs();
	void spiral();
	void rays();
	void figCirc();
	void spectrum();
	void confetti();
	void lines();
	void tunel();
	void tentacles();

	/// VAN OTRAS
	void volConcentrico();
	void colorRainhaha();

	/// CON VIDEO
	void camara();

	//////// FBO MASCARILLA Y AYUDA ////////
	ofFbo fboMaster;		// fbo Principal con mascarilla
	ofFbo fboHelp;			// fbo Menu de ayuda
	ofFbo fboGenMask;			// fbo Menu de ayuda

	//////// FUNCIONES MASCARILLA Y AYUDA ////////
	void mascarilla();
	void helpF();
	void genMaskF();

};
