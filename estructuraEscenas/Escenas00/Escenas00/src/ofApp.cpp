#include "ofApp.h"

//--------------------------------------------------------------
void ofApp::setup() {
	ofSetVerticalSync(true); // hace refresh de contenidos graficos en sincronia con la pantalla Hz
	radioCam = ofGetHeight()/2;

}

//--------------------------------------------------------------
void ofApp::update() {
	if (entra) {
		if (opaIni < 255)
			opaIni += velOpaT;
	}
	if (sale) {
		if (opaIni > 0)
			opaIni -= velOpaT;
	}
}

//--------------------------------------------------------------
void ofApp::draw() {
	ofBackground(0);
	ofTranslate(ofGetWidth() / 2, ofGetHeight() / 2);
	
	ofSetColor(255, opaIni);
	ofFill();
	ofSetPolyMode(OF_POLY_WINDING_POSITIVE);
	ofBeginShape();
	for (int i = 0; i < numSecc; i++) {
		ofVertex(cos(HALF_PI + (TWO_PI/numSecc)*i)*radioCam, sin(HALF_PI + (TWO_PI/ numSecc)*i)*radioCam);
	}
	ofEndShape();
	
	//ofCircle(0,0, ofGetHeight()/2*zoomCont);
	
	escenas();


}
//--------------------------------------------------------------
void ofApp::escenas() {

	switch (escenaAct) {
	case 0:

		ofPushStyle();

		ofSetColor(255, opaIni);
		ofFill();

		ofDrawBitmapString("Test", 100, 100);

		ofTranslate(ofGetWidth() / 2, ofGetHeight() / 2);
		ofSetRectMode(OF_RECTMODE_CENTER);

	ofRect(0, 0, 100, 100);

		ofPopStyle();

		break;
	case 1:
		break;
	case 2:
		break;
	case 3:
		break;
	case 4:
		break;
	case 5:
		break;
	}
}

//--------------------------------------------------------------
void ofApp::keyPressed(int key) {
	if (key == 'a')
		numSecc++;
	if (key == 's')
		numSecc--;
}

//--------------------------------------------------------------
void ofApp::keyReleased(int key) {

}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y) {

}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button) {

}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button) {

}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button) {

}

//--------------------------------------------------------------
void ofApp::mouseEntered(int x, int y) {

}

//--------------------------------------------------------------
void ofApp::mouseExited(int x, int y) {

}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h) {

}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg) {

}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo) {

}
