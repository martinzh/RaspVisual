#pragma once

#include "ofMain.h"
#include <winsock2.h>

class ofApp : public ofBaseApp {

public:
	void setup();
	void update();
	void draw();

	void keyPressed(int key);
	void keyReleased(int key);
	void mouseMoved(int x, int y);
	void mouseDragged(int x, int y, int button);
	void mousePressed(int x, int y, int button);
	void mouseReleased(int x, int y, int button);
	void mouseEntered(int x, int y);
	void mouseExited(int x, int y);
	void windowResized(int w, int h);
	void dragEvent(ofDragInfo dragInfo);
	void gotMessage(ofMessage msg);

	// ESCENAS
	void escenas();
	int numEscenas = 6;
	int escenaAct = 0;

	// OPACIDADES
	float opaIni = 0.0f;
	float opaSal = 0.0f;
	float velOpaT = 0.5f; // velocidad de transcion
	bool entra = true, sale;	  // BOOLEANS ENTRA  - E

	float zoomCont = 1.0f;
	int numSecc = 3;
	int radioCam;
};
